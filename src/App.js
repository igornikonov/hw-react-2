import React, { Component } from 'react';
import './App.scss';
import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';
import { modalWindowDeclarations } from './Components/Modal/ModalConfig';
import Header from './Components/Header/Header';
import ItemCard from './Components/ItemCard/ItemCard';

class App extends Component {
  state = {
    noModals: true,
    shopItems: [],
    currentItemId: null,
    itemsInCart: JSON.parse(localStorage.getItem('itemsInCart')) || [],
    favourites: JSON.parse(localStorage.getItem('favourites')) || [],
    modalContent: {},
  };

  componentDidMount() {
    fetch('shopItems.json')
      .then(res => res.json())
      .then(data => {
        this.setState({ shopItems: data });
      });
  }

  openModalHandler = (e) => {
    const modalID = e.target.id;
    const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);
    this.setState((prev) => {
      return {
        noModals: !prev.noModals,
        modalContent: modalDeclaration,
        currentItemId: e.target.parentNode.id
      }
    });
  };
  closeModalHandler = () => {
    this.setState((prev) => {
      return {
        noModals: !prev.noModals,
        modalContent: {},
        currentItemId: null,
      }
    });
  };
  




  
  addToCartHandler = (id) => {
    const { shopItems } = this.state;

    const itemsInCart = JSON.parse(localStorage.getItem('itemsInCart')) || [];
    const itemToAdd = shopItems.find(item => item.id.toString() === id.toString());
    itemsInCart.push(itemToAdd.id);

    localStorage.setItem('itemsInCart', JSON.stringify(itemsInCart));
    this.setState({ itemsInCart: itemsInCart });

    this.closeModalHandler();
  };

  addToFavouritesHandler = (id) => {
    const favouriteItems = JSON.parse(localStorage.getItem('favourites')) || [];

    if (!favouriteItems.includes(id)) {
      favouriteItems.push(id);
    } else {
      for (let i = 0; i < favouriteItems.length; i++) {
        if (favouriteItems[i] === id.toString()) {
          favouriteItems.splice(i, 1);
        }
      }
    }

    localStorage.setItem('favourites', JSON.stringify(favouriteItems));
    this.setState({ favourites: favouriteItems });
  }

  render() {
    const { noModals, shopItems, currentItemId, favourites } = this.state;
    const { title, description, backgroundColor, headerBackgroundColor } = this.state.modalContent;
    const renderedShopItems = shopItems.map(({ name, id, brand, picture, price }) =>
      <ItemCard
        key={id}
        id={id}
        name={name}
        brand={brand}
        picture={picture}
        price={price}
        addToFavourites={this.addToFavouritesHandler}
        openModal={this.openModalHandler}
        favourites={favourites}
      />
    )

    if (noModals) {
      return (
        <>
          <Header />
          <ul className="shop-items-container">
            {renderedShopItems}
          </ul>
        </>
      );
    }
    return (
      <>
        <Modal
          header={title}
          text={description}
          backgroundColor={backgroundColor}
          headerBackgroundColor={headerBackgroundColor}
          closeModal={this.closeModalHandler}
          addToCart={this.addToCartHandler}
          id={currentItemId}
        />
      </>
    )
  }
}

export default App;
