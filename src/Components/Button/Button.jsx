import React, { Component } from 'react';
import './Button.scss';
import propTypes from 'prop-types';

class Button extends Component {

  render() {
    const { text, onClick, btnID, className } = this.props;

    return (
      <button
        id={btnID}
        className={className}
        onClick={(btnID) => onClick(btnID)}
      >
        {text}
      </button>
    )
  }
}

Button.propTypes = {
  text: propTypes.string.isRequired,
  onClick: propTypes.func.isRequired,
  className: propTypes.string,
  btnID: propTypes.string.isRequired
}

Button.defaultProps = {
  className: 'btn',
  btnID: ''
}

export default Button;