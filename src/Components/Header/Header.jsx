import React, { Component } from 'react';
import './Header.scss';
import headerImg from './header.jpg';

class Header extends Component {

  render() {
    return (
      <img src={headerImg} alt="a bike shop banner" className="header-img" />
    );
  };
}

export default Header;